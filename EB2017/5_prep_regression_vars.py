#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>

import pandas
import pickle
import numpy
# Give very basic progress bars.
from tqdm import tqdm
tqdm.pandas()
'''
Create three files from dataframes:
Explanatory_Vars.pickle
Dependent_Vars.pickle
Control_Vars.pickle
'''

# Disable warning:
pandas.set_option('chained_assignment', None)
'''
This disables this warning:
prep_explanatory_vars.py:58: SettingWithCopyWarning:
A value is trying to be set on a copy of a slice from a DataFrame.
Try using .loc[row_indexer,col_indexer] = value instead
--
See 'TODO:' below.

'''


def main():
    # Import Question Set.
    for model in range(1, 6):
        model_vars = run_model(model)
        print "Writing Model Vars", model
        model_vars.to_pickle('pickles/Model_{}_vars.pickle'.format(model))


def run_model(model):
    print 'Loading Data...'
    filename = 'reproduction_data/Model_{}.pickle'.format(model)
    data = pandas.read_pickle(filename)
    explanatory = get_explanatory_vars(data)
    dependent = get_dependent_vars(data)
    control = get_control_vars(data)
    df = pandas.concat([explanatory, dependent, control], axis=1)
    return df


def get_explanatory_vars(data):
    Explanatory_Vars = explanatory_vars(data)
    return Explanatory_Vars


def get_dependent_vars(data):
    Dependent_Vars = dependent_vars(data)
    return Dependent_Vars


def get_control_vars(data):
    Control_Vars = control_vars(data)
    return Control_Vars


def explanatory_vars(data):
    Explanatory_Vars = pandas.DataFrame()

    # Create Dummy Variables for Cognative Motivation.
    CM = data['POL DISCUSSION - FREQUENCY']
    print "Cognative Motivation."
    Explanatory_Vars['CM_FREQUENTLY'] = CM.progress_apply(cm_frequently)
    Explanatory_Vars['CM_OCCASIONALLY'] = CM.progress_apply(cm_occassionally)
    Explanatory_Vars['CM_NEVER'] = CM.progress_apply(cm_never)

    # Create Dummy Var for Political Values.
    print "Political Values."
    PV = data[['VALUE ORIENTATION - POL GOALS 1ST',
               'VALUE ORIENTATION - POL GOALS 2ND']]
    Explanatory_Vars['MATERIALIST'] = PV.progress_apply(is_materialist, axis=1)
    Explanatory_Vars[
        'POST_MATERIALIST'] = PV.progress_apply(is_postmaterialist, axis=1)

    # Create Vars for Utilitarian Appraisals of Integrative Policy
    # 1. Educational Level.
    print 'Educational Level.'
    edu_lvl = data[['AGE EXACT', 'AGE EDUCATION']]
    # make age numeric.
    # TODO: Use set on copy in a way that doesn't cause a warning.
    edu_lvl['AGE EXACT'] = edu_lvl['AGE EXACT'].progress_apply(age_numeric)
    # Create Dummy Vars for Low/Low-Mid/High-Mid/High Edu Level.
    Explanatory_Vars[
        'EDU_LOW'] = edu_lvl.progress_apply(low_ed, axis=1)
    Explanatory_Vars[
        'EDU_LOW_MID'] = edu_lvl.progress_apply(low_mid_ed, axis=1)
    Explanatory_Vars[
        'EDU_HIGH_MID'] = edu_lvl.progress_apply(high_mid_ed, axis=1)
    Explanatory_Vars[
        'EDU_HIGH'] = edu_lvl.progress_apply(high_ed, axis=1)
    # 2. Income
    income_dummy = pandas.get_dummies(data['iquartile'])
    income_dummy.columns = [
        'INC_LOW',
        'INC_LOW_MID',
        'INC_HIGH_MID',
        'INC_HIGH']
    Explanatory_Vars = pandas.concat([Explanatory_Vars, income_dummy], axis=1)
    # 3. Occupation
    temp_df = pandas.DataFrame()
    OCCUPATION = data[['OCCUPATION OF RESPONDENT', 'Study Number']]
    temp_df['OCCUPATION'] = OCCUPATION.progress_apply(
        code_occupation, axis=1)
    dummy = pandas.get_dummies(temp_df['OCCUPATION'])
    dummy.columns = ['FARMER',
                     'PROFESSIONAL',
                     'MANUAL',
                     'OWNER',
                     'WHITE_COLLAR',
                     'EXECUTIVE',
                     'RETIRED',
                     'HOUSEWIFE',
                     'STUD_MIL',
                     'UNEMPLOYED']
    Explanatory_Vars = pandas.concat([Explanatory_Vars, dummy], axis=1)

    # 4. Vote Intention - Support For Government
    vi_data = pickle.load(open('pickles/VoterIntention.pickle'))
    gov_lookup = pickle.load(open('pickles/gov_lookup.pickle'))
    VI = data[
        ['VOTE INTENTION', 'YEAR', 'isocntry', 'Study Number']]
    Explanatory_Vars['SUPPORT_FOR_GOV'] = VI.progress_apply(
        support_for_gov, args=(vi_data, gov_lookup), axis = 1)

    # Class Partisanship - Vote Intention Prol/Bour
    cp_lookup = pickle.load(open('pickles/cp.pickle', 'r'))
    CP = data[
        ['VOTE INTENTION', 'YEAR', 'isocntry', 'Study Number']]
    Explanatory_Vars['SUPPORT_PROL'] = CP.progress_apply(
        class_partisanship_prol, args=(vi_data, cp_lookup), axis = 1)
    Explanatory_Vars['SUPPORT_BOUR'] = CP.progress_apply(
        class_partisanship_bour, args=(vi_data, cp_lookup), axis = 1)
    return Explanatory_Vars


def dependent_vars(data):
    Dependent_Vars = pandas.DataFrame()
    # Create Var for EU Good/Bad.
    print 'Membership:'
    Dependent_Vars['MEMBERSHIP'] = data[
        'EC MEMBERSHIP GOOD-BAD'].progress_apply(membership)
    print 'Unify:'
    # Create Var for EU Good/Bad.
    Dependent_Vars['UNIFY'] = data[
        'EUROPEAN UNIFICATION FOR-AGAINST'].progress_apply(unify)
    print 'Support:'
    # Create Var for Support (Membership + Unify)
    Dependent_Vars['SUPPORT'] = Dependent_Vars[
        ['MEMBERSHIP', 'UNIFY']].progress_apply(support, axis=1)
    # We only need 'SUPPORT' from this point on.
    del Dependent_Vars['UNIFY']
    del Dependent_Vars['MEMBERSHIP']
    return Dependent_Vars


def control_vars(data):
    Control_Vars = pandas.DataFrame()
    # Add dummies for YEAR
    print 'Year:'
    YEAR = pandas.get_dummies(data.YEAR)
    Control_Vars = pandas.concat([Control_Vars, YEAR], axis=1)
    print 'Woman:'
    # Add dummies for is woman.
    Control_Vars['WOMAN'] = data.SEX.progress_apply(is_woman)
    print 'European Weight:'
    # Add European Weight
    Control_Vars['EUROPEAN WEIGHT'] = data['EUROPEAN WEIGHT']
    # Add Country
    # Control_Vars['COUNTRY'] = data['isocntry']
    print 'Age:'
    # Create a Var for Age.
    # Numeric representation of actual Age.
    # Input data has mix of numbers and '15 YEARS OLD' so needs cleaning.
    Control_Vars['AGE'] = data['AGE EXACT'].progress_apply(age_numeric)
    # Create dummies for country.
    countries = pandas.get_dummies(data['isocntry'])
    Control_Vars = pandas.concat([Control_Vars, countries], axis=1)
    return Control_Vars


def low_ed(x):
    Low = [
        '14 YEARS - YOUNGER',
        '14 YEARS AND LESS',
        '14 YEARS OR YOUNGER',
        'UP TO 14 YEARS',
        'Up to 14 Years',
        'UP TO 15 YEARS']
    Still_Studying = ['STILL STUDYING', 'Still Studying', 'SILL STUDYING']
    if x['AGE EDUCATION'] in Low:
        return 1
    elif (x['AGE EDUCATION'] in Still_Studying) and (x['AGE EXACT'] < 15):
        return 1


def low_mid_ed(x):
    Low_Mid = [
        '15 Years',
        '15 YEARS',
        '16 Years',
        '16 YEARS',
        '17 YEARS',
        '17 Years',
        '18 YEARS',
        '18 Years',
        '19 Years',
        '19 YEARS',
        '16 - 19 YEARS']
    Still_Studying = ['STILL STUDYING', 'Still Studying', 'SILL STUDYING']
    if x['AGE EDUCATION'] in Low_Mid:
        return 1
    elif ((x['AGE EDUCATION'] in Still_Studying) and
            (15 <= x['AGE EXACT'] <= 19)):
        return 1


def high_mid_ed(x):
    High_Mid = ['20 YEARS', '20 Years', '20 + YEARS', '21 YEARS', '21 Years']
    Still_Studying = ['STILL STUDYING', 'Still Studying', 'SILL STUDYING']
    if x['AGE EDUCATION'] in High_Mid:
        return 1
    elif ((x['AGE EDUCATION'] in Still_Studying) and
            (20 <= x['AGE EXACT'] <= 21)):
        return 1


def high_ed(x):
    High = [
        '22 YEARS OO OLDER',
        '22 YEARS',
        '22 Years and Older',
        '22 Y OR OLDER',
        '22 YEARS AND OLDER',
        '22 YEARS OR OLDER',
        '22 YRS OR OLDER',
        '22 YEARS AND MORE',
        '22 YEARS UND OLDER',
        '22 YEARS, OLDER']
    Still_Studying = ['STILL STUDYING', 'Still Studying', 'SILL STUDYING']
    if x['AGE EDUCATION'] in High:
        return 1
    elif (x['AGE EDUCATION'] in Still_Studying) and (22 <= x['AGE EXACT']):
        return 1


def age_numeric(x):
    # Convert value to string and make all upper-case.
    test_string = str(x).upper()
    # If value is nan, preserve nan.
    if test_string == 'NAN':
        return x
    # If string contains letters.
    elif test_string.isupper():
        # Remove letters, return numbers, as a float.
        rc = filter(lambda x: x.isdigit(), test_string)
        return float(rc)
    # If nothing else applies, return the value we have as it's already a
    # number.
    else:
        return x


def support(x):
    return x['MEMBERSHIP'] + x['UNIFY']


def unify(x):
    For_Very_Much = ['FOR VERY MUCH',
                     'FOR- VERY MUCH',
                     'FOR-VERY MUCH',
                     'FOR -- VERY MUCH',
                     'FOR - VERY MUCH',
                     'FOR -VERY MUCH',
                     'VERY MUCH FOR',
                     'For - Very Much']

    For_To_Some_Extent = ['FOR TO SOME EXTENT',
                          'FOR- TO SOME EXTENT',
                          'FOR-TO SOME EXTENT',
                          'FOR-TO SOME EXTE',
                          'FOR -TO SOME EXT',
                          'FOR- SOME EXTENT',
                          'FOR - SOME EXTNT',
                          'FOR - SOME EXTENT',
                          'SOME EXTENT FOR',
                          'For - To Some Extent',
                          'FOR - TO SOME EXTENT',
                          'FOR SOME EXTENT']

    Against_To_Some_Extent = ['AGAINST TO SOME EXT',
                              'AGAINST- TO SOME EXTENT',
                              'AGAINST-TO SOME EXTENT',
                              'AGAINST-TO SOME',
                              'AGAINST -TO SOME',
                              'AGAINST- SOME EX',
                              'AGAINST - SOME EXTENT',
                              'AGNST - SOME EXT',
                              'AGNST TO SME EXT',
                              'AGAINST - SOME EXT',
                              'SOME EXT AGAINST',
                              'SOME EXTENT AGAINST',
                              'AGAINST TO SOME EXTENT',
                              'Against-To Some Extent',
                              'AGAINST - TO SOME EXTENT',
                              'AGAINST SOME EXTENT']

    Against_Very_Much = ['AGAINST VERY MUCH',
                         'AGAINST- VERY MUCH',
                         'AGAINST-VERY MUC',
                         'AGAINST -VERY MU',
                         'AGAINST-VERY MUCH',
                         'AGNST - VERY MUCH',
                         'AGAINST - VERY MUCH',
                         'MUCH AGAINST',
                         'VERY MUCH AGAINST',
                         'Against - Very Much',
                         'AGAINST - VERY MUCH']

    if x in For_Very_Much:
        return 4
    if x in For_To_Some_Extent:
        return 3
    if x in Against_To_Some_Extent:
        return 2
    if x in Against_Very_Much:
        return 1
    else:
        # NaN coded as 'Don't Know'.
        return 2.5


def membership(x):
    Good = ['GOOD THING', 'A GOOD THING', 'A Good Thing']
    Neither = [
        'NEITHER GOOD NOR BAD',
        'NEITHER NOR',
        'NEITHER NOR / incl. DK, NA',
        'NEITHER GOOD,BAD',
        'NEITHER GOOD NOR',
        'NOT GOOD OR BAD',
        'NEITHER GOOD-BAD',
        'NTHR GOOD NOR BD',
        'NEITHR GOOD,BAD',
        'NEITHER'
        'Neither Nor']
    Bad = ['BAD THING', 'A BAD THING', 'A Bad Thing']
    if x in Bad:
        return 1
    if x in Neither:
        return 2
    if x in Good:
        return 3
    else:
        # NaN + number = NaN. Must return a number.
        return 0


def is_materialist(x):
    Maint_Order = [
        u'MAINT ORDR NTION',
        u'MAINTAIN NAT ORD',
        u'MAINTAIN ORDER',
        u'MAINTAINING ORDE',
        u'MAINTAINING ORDER',
        u'MAINTAINNG ORDER',
        u'MAINTENANCE LAW/ORDER',
        u'Maintaining Order']

    Rising_Prices = [
        u'FGHT RSNG PRICES',
        u'FGT RISING PRCS',
        u'FIGHT PRICE INC',
        u'FIGHT RISE PRICE',
        u'FIGHT RISING PRICES',
        u'FIGHT RISNG PRIC',
        u'FIGHTING RISING PRICES',
        u'Fight Rising Prices']

    order = False
    prices = False

    if ((x['VALUE ORIENTATION - POL GOALS 1ST'] in Maint_Order)
            or (x['VALUE ORIENTATION - POL GOALS 2ND'] in Maint_Order)):
        order = True
    if ((x['VALUE ORIENTATION - POL GOALS 1ST'] in Rising_Prices)
            or (x['VALUE ORIENTATION - POL GOALS 2ND'] in Rising_Prices)):
        prices = True

    if (order) and (prices):
        return 1


def is_postmaterialist(x):
    More_Say = [
        u'GIV PPL MORE SAY',
        u'GIVE PEOPLE MORE SAY',
        u'GIVE PPLE MO SAY',
        u'GIVING PEOPLE MORE SAY',
        u'GVNG PPL MRE SAY',
        u'MORE SAY GOV DEC',
        u'MORE SAY GOV DECISIONS',
        u'MORE SAY IN GVRNMNT',
        u'PEOPLE MORE SAY',
        u'People More Say']

    Free_Speech = [
        u'FREE SPEECH',
        u'FREEDOM OF EXPRESSION',
        u'FREEDOM OF SPEECH',
        u'Freedom of Speech',
        u'PROTCT FREE SPCH',
        u'PROTECT FREE SPC',
        u'PROTECT FREE SPE',
        u'PROTECT FREEDOM OF SPEECH',
        u'PROTECTING FREEDOM',
        u'PRTCNG FRDM SPCH',
        u'PRTCT FRDM SPCH']

    say = False
    freedom = False

    if ((x['VALUE ORIENTATION - POL GOALS 1ST'] in More_Say)
            or (x['VALUE ORIENTATION - POL GOALS 2ND'] in More_Say)):
        say = True
    if ((x['VALUE ORIENTATION - POL GOALS 1ST'] in Free_Speech)
            or (x['VALUE ORIENTATION - POL GOALS 2ND'] in Free_Speech)):
        freedom = True

    if (say) and (freedom):
        return 1


def cm_frequently(x):
    frequently = [u'FREQUENTLY', u'FRQUENTLY', u'Frequently', u'OFTEN']
    if x in frequently:
        return 1


def cm_occassionally(x):
    occassionally = ['OCCASIONALLY', u'Occasionally', u'FROM TIME TO TIME']
    if x in occassionally:
        return 1


def cm_never(x):
    never = [u'NEVER', u'Never']
    if x in never:
        return 1


def is_woman(x):
    woman = [u'FEMALE', u'Female', u'WOMAN']
    if x in woman:
        return 1


def support_for_gov(x, vi_data, gov_lookup):
    support = 0
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # During the period under the study the people of Northern Ireland didn't
    # vote for parties that ruled the UK. Omit GB-NIR.
    if x.isocntry == 'GB-NIR':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    # print x.YEAR, x['Study Number'], vi_country
    a = gov_lookup[str(x.YEAR)][x['Study Number']][vi_country]['eb-match']
    # To make comparison possible, normalize strings. (Only Alpha, all-caps.)
    preferred_party = alpha_only([preferred_party])
    gov_party = alpha_only([a])
    if gov_party[0] in preferred_party[0]:
        support = 1
    return support


def alpha_only(name_list):
    # Normalize country names:
    temp_list = []
    for name in name_list:
        alpha_only_string = ''.join([i for i in name if i.isalpha()])
        temp_list.append(alpha_only_string.upper())
    return temp_list


def low_inc(x):
    Low = [
        '- -',
        'LOW',
        'LOWEST QUARTILE',
        'LOWEST',
        '<LOWEST QUARTILE>',
        'POOREST']
    if x['INCOME QUARTILES'] in Low:
        return 1


def low_mid_inc(x):
    Low_Mid = [
        '2',
        '-',
        'NEXT TO LOWEST',
        '<NEXT TO LOWEST>',
        'NEXT TO POOREST',
        'NEXT TO LOW']
    if x['INCOME QUARTILES'] in Low_Mid:
        return 1


def high_mid_inc(x):
    High_Mid = [
        '.',  # ZA1208 Appears to encode High Mid quartile as '.'.
        '3',
        '+',
        'NEXT TO HIGHEST',
        '<NEXT TO HIGHEST>',
        'NEXT TO RICHEST',
        'NEXT TO HIGH']
    if x['INCOME QUARTILES'] in High_Mid:
        return 1


def high_inc(x):
    High = ['+ +', 'HIGH', 'HIGHEST QUARTILE', '<HIGHEST QUARTILE>', 'RICHEST']
    if x['INCOME QUARTILES'] in High:
        return 1


def class_partisanship_prol(x, vi_data, cp_lookup):
    p = 0
    prol = cp_lookup['prol']
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # During the period under the study the people of Northern Ireland didn't
    # vote for parties that ruled the UK. Omit GB-NIR.
    if x.isocntry == 'GB-NIR':
        return 0
    # No East German Parties are defined as Prol or Bour parties. Omitting.
    elif x.isocntry == 'DE-E':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    if preferred_party in prol[x['Study Number']][vi_country]:
        # print 'prol', x['Study Number'], [vi_country], preferred_party
        p = 1
    return p


def class_partisanship_bour(x, vi_data, cp_lookup):
    b = 0
    bour = cp_lookup['bour']
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # During the period under the study the people of Northern Ireland didn't
    # vote for parties that ruled the UK. Omit GB-NIR.
    if x.isocntry == 'GB-NIR':
        return 0
    # No East German Parties are defined as Prol or Bour parties. Omitting.
    elif x.isocntry == 'DE-E':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    if vi_country in bour[x['Study Number']]:
        if preferred_party in bour[x['Study Number']][vi_country]:
            # print 'bour', x['Study Number'], [vi_country], preferred_party
            b = 1
    return b


def code_occupation(x):
    farmer = [
        'FARMERS/FISHERMEN',
        'FARMER, FISHERMAN',
        'FARMERS,FISHERM',
        'FARMERS,FISHERME',
        'FARMERS FISHERMEN',
        'FARMER/FISHERMAN',
        'FARMER',
        'Farmer',
        'Fisherman',
        'FARMER FISHERMAN',
        'FISHERMAN']

    professional = [
        'PROFESSIONAL',
        'PROFESSIONAL -LA',
        'PFNL-LWYR,ACCTNT',
        'EMPLOYED PROFESSIONAL',
        'EMPLOYED PROFESS',
        'Professional',
        'Employed Professional']

    manual = [
        'MANUAL WORKERS',
        'MANUAL WORKER',
        'OTHER MANUAL WORKER',
        'OTHER MANUAL WORKERS',
        'Other Manual Workers',
        'OTH MANUAL WORKER',
        'SKILLED MANUAL WORKER',
        'SKILLED MANUAL W',
        'Skilled Manual Workers',
        'SKILLED MANUAL WORKERS']

    owner = [
        'BUSINESS OWNERS',
        'BUSINESS'
        'BUSINESS -OWNERS',
        'BUSINESS OWNER',
        'BSNS-SHPS,CFT,PR',
        'SHOPOWNER/CRAFTSMEN',
        'OWNERS OF SHOPS OR CAMPANIES',
        'OWNERS SHOPS COMPANIES',
        'Business Owners',
        'BUSINESS PROPRIETORS',
        'OWNERS OF SHOPS',
        'SHOP OWNER, CRAFTSMEN']

    white_collar = [
        'WHITE COLLAR',
        'WHITE COLLAR -OF',
        'WHITE COL OF WKR',
        'WHITE COLLAR WORKER',
        'NON-OFFICE EMPLOYEES',
        'MIDDLE MANAGEMENT',
        'Other Office Employees',
        'OTHER OFFICE EMPLOYEES',
        'SUPERVISORS',
        'MIDDLE MANAGEMEN',
        'OTH OFFICE EMPLOY',
        'NON-OFFICE EMPLOY',
        'SUPERVISOR',
        'EMPLOYED SERVICE JOB',
        'OTHER OFFICE',
        'Middle Management',
        'Supervisors',
        'Non Office Employees',
        'NON OFFICE EMPLOYEES',
        'EMPLOYED AT A DESK',
        'EMPLOYED TRAVELLING']

    executive = [
        'EXECUTIVES',
        'EXECUTIVE',
        'EXECUTIVE,TOP MA',
        'EXECUTIVE TOP MANAG',
        'GENERAL MANAGEMENT',
        'GENERAL MANAGEME',
        'General Management']

    retired = ['RETIRED', 'RETITRED', 'Retired']

    housewife = [
        'HOUSEWIVES',
        'HOUSEWIFE',
        'HOUSEWIFE,NOT EM',
        'HSWF,NT OTHW EMP',
        'LOOKING AFTER HH',
        'HSWF NT OTHW EMP',
        'HOUSEWIFE NOT EMPL',
        'Housewife',
        'HOUSEWIFE NOT EMPLOYED']

    stud_mil = [
        'STUDENT/MILIT SERV',
        'STUDENT, MILIT SERV',
        'STUDENT,MILITARY',
        'STUDENT MILIT SERV',
        'Student',
        'MILITARY',
        'STUDENT',
        'Military Service',
        'MILITARY SERVICE']

    unemployed = ['UNEMPLOYED <DK NA>', 'UNEMPLOYMENT', 'UNEMPLOYED',
                  'TEMP UNEMPLOYED', 'Temp Unemployed', 'UNEMPLOYED TEMPORARLY']

    # HANDLE NAN CASE.
    occupation = x['OCCUPATION OF RESPONDENT']
    if occupation in farmer:
        return 1
    elif occupation in professional:
        return 2
    elif occupation in manual:
        return 3
    elif occupation in owner:
        return 4
    elif occupation in white_collar:
        return 5
    elif occupation in executive:
        return 6
    elif occupation in retired:
        return 7
    elif occupation in housewife:
        return 8
    elif occupation in stud_mil:
        return 9
    elif occupation in unemployed:
        return 10
    else:
        return numpy.NAN


if __name__ == '__main__':
    main()
