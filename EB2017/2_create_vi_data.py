#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>
# This file creates vi_data.pickle which contains the mapping of party to
# numerical code in eb data. The original contains some errors (Finana Fail
# spelt Finna Fael. For this reason vi_data.py is used in the pipeline, which is
# the same data from the pickle, put into a python dictionary and manually
# corrected in these cases.

import pandas
import pickle


def main():
    survey = pandas.read_csv("EurobarometerData78-92.csv")
    # List of surveys.
    surveys = survey.columns.drop('Study Number')
    # Voter Intention:
    VI = survey.iloc[1]
    vi_data = {}
    for eb in surveys:
        f = open('eb78-92/txt/' + eb + '_cdb.pdf.txt', "r")
        data = f.readlines()
        print eb, VI[eb]
        vi_data[eb] = survey_to_parties(data, VI[eb])
    vi_data = data_cleanup(vi_data)

    outfile = 'pickles/VoterIntention.pickle'
    print 'Writing file:', outfile
    pickle.dump(vi_data, open(outfile, 'wb'))
    # To Recover data:
    # import pickle
    # vi_data = pickle.load(open('VoterIntention.pickle'))
    # vi_data['ZA1541']['IE']


def data_cleanup(vi_data):
    # Data Cleanup.
    for survey in vi_data:
        # Data has inconsistencies in how w. germany is coded.
        # Always code as DE-W.
        if 'DE_E' in vi_data[survey].keys():
            vi_data[survey]['DE-E'] = vi_data[survey].pop('DE_E')
        if 'DE_W' in vi_data[survey].keys():
            vi_data[survey]['DE-W'] = vi_data[survey].pop('DE_W')
        if 'DE' in vi_data[survey].keys():
            vi_data[survey]['DE-W'] = vi_data[survey].pop('DE')
        # Norway covered by some surveys but beyond scope for this work.
        if 'NO' in vi_data[survey].keys():
            vi_data[survey].pop('NO')
        # All stata data encodes areas of the UK with a '-', the codebooks
        # contain a mix of '-' and '_'. This normalizes that behavriour 'GB_GBN'
        # 'GB_NIR'.
        if 'GB_GBN' in vi_data[survey].keys():
            vi_data[survey]['GB-GBN'] = vi_data[survey].pop('GB_GBN')
        if 'GB_NIR' in vi_data[survey].keys():
            vi_data[survey]['GB-NIR'] = vi_data[survey].pop('GB_NIR')

    # In some EB Codebooks Fianna Fail is recorded as Fianna Fael, this makes
    # comparison with EBI data impossibl and so is fixed here.
    for study in vi_data.keys():
        for code, party in vi_data[study]['IE'].iteritems():
            if 'FAEL' in party.upper():
                vi_data[study]['IE'][code] = 'FIANNA FAIL'
    return vi_data


def survey_to_parties(data, var_number):
    beginning, end = get_range(data, var_number)
    question_data = get_question_data(beginning, end, data)
    index = get_index(beginning, end, question_data)

    for country in index.keys():
        index[country] = get_parties(
            country,
            index[country],
            question_data)

    return index


def get_parties(country, cursors, question_data):
    parties = {}
    for line in range(cursors['beginning'], cursors['end']):
        if question_data[line][0].isdigit():
            code, party = question_data[line].split(' ', 1)
            parties[code] = party
    return parties


def get_index(beginning, end, question_data):
    index = {}
    for idx, line in enumerate(question_data):
        # Finds lines like 'in France:' followed by answers, '10 LABOR'
        if line.startswith('in ') and question_data[(idx + 1)][0].isdigit():
            # Extract Country Code, use as Key.
            line = line[line.find("(") + 1:line.find(")")]
            index[line] = idx
    countries = index.keys()
    cursors = index.values()
    cursors.append(len(question_data))
    cursors.sort()
    for country in countries:
        beginning = index[country]
        end = cursors[cursors.index(beginning) + 1]
        index[country] = {'beginning': beginning, 'end': end}
    return index


def get_question_data(beginning, end, data):
    question_data = []
    for i in range(beginning, end):
        if data[i] != '\n':
            if 'Eurobarometer' not in data[i]:
                if 'printed' not in data[i]:
                    if 'page' not in data[i]:
                        if 'GESIS' not in data[i]:
                            question_data.append(data[i].rstrip())
    return question_data


def get_range(data, string):
    return_list = []
    for idx, item in enumerate(data):
        if item.startswith(string):
            return_list.append(idx)
    return return_list[:2]


if __name__ == '__main__':
    main()
