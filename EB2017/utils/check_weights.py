#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>
import pandas


def main():
    data = pandas.read_pickle('reproduction_data/all_data.pickle')
    for study in data['Study Number'].unique():
        print study
        study_data = data[data['Study Number'] == study]
        for country in study_data.isocntry.unique():
            if country == 'LU':
                print country,
                print study_data[study_data.isocntry == country]['EUROPEAN WEIGHT'].unique()
        print ""


if __name__ == '__main__':
    main()
