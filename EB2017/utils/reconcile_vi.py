#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>
import pickle
import pandas


def main():
    vi_data = pickle.load(open('VoterIntention.pickle'))
    eb_data = pandas.read_pickle('reproduction_data/all_data.pickle')
    eb_list = eb_data['Study Number'].unique()
    superset = set()
    for study in eb_list:
        vi_country = vi_data[study].keys()
        eb_country = eb_data[eb_data['Study Number'] == study].isocntry.unique()
        print study
        # print vi_country
        # print eb_country
        vi_country = set(vi_country)
        eb_country = set(eb_country)
        superset = superset.union(eb_country)

        VIminusEB = vi_country - eb_country
        EBminusVI = eb_country - vi_country

        # if len(VIminusEB) != 0 or len(EBminusVI) != 0:
        #     print study, 'VI-EB:', VIminusEB, 'EB-VI:', EBminusVI
    print superset

def alpha_only(name_list):
        # Normalize country names:
        temp_list = []
        for name in name_list:
            alpha_only_string = ''.join([i for i in name if i.isalpha()])
            temp_list.append(alpha_only_string)
        return temp_list


if __name__ == '__main__':
    main()
