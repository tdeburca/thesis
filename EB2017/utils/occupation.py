#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright © 2017-06-13 Tiarnan de Burca <tdeburca@gmail.com>
import pandas
import numpy
import pickle
# Give very basic progress bars.
from tqdm import tqdm
tqdm.pandas()
pandas.set_option('chained_assignment', None)


def main():
    data = pandas.read_pickle('reproduction_data/all_data.pickle')
    Explanatory_Vars = pandas.DataFrame()
    OCCUPATION = data[['OCCUPATION OF RESPONDENT', 'Study Number']]
    Explanatory_Vars['OCCUPATION'] = OCCUPATION.progress_apply(
        recode_occupation, axis=1)
    print 'nulls', Explanatory_Vars.isnull().sum()
    dummy = pandas.get_dummies(Explanatory_Vars['OCCUPATION'])
    dummy.columns = ['farmer',
                     'professional',
                     'manual',
                     'owner',
                     'white_collar',
                     'executive',
                     'retired',
                     'housewife',
                     'stud_mil',
                     'unemployed']
    Explanatory_Vars = dummy
    print Explanatory_Vars.head(20)


def recode_occupation(x):
    farmer = [
        'FARMERS/FISHERMEN',
        'FARMER, FISHERMAN',
        'FARMERS,FISHERM',
        'FARMERS,FISHERME',
        'FARMERS FISHERMEN',
        'FARMER/FISHERMAN',
        'FARMER',
        'Farmer',
        'Fisherman',
        'FARMER FISHERMAN',
        'FISHERMAN']

    professional = [
        'PROFESSIONAL',
        'PROFESSIONAL -LA',
        'PFNL-LWYR,ACCTNT',
        'EMPLOYED PROFESSIONAL',
        'EMPLOYED PROFESS',
        'Professional',
        'Employed Professional']

    manual = [
        'MANUAL WORKERS',
        'MANUAL WORKER',
        'OTHER MANUAL WORKER',
        'OTHER MANUAL WORKERS',
        'Other Manual Workers',
        'OTH MANUAL WORKER',
        'SKILLED MANUAL WORKER',
        'SKILLED MANUAL W',
        'Skilled Manual Workers',
        'SKILLED MANUAL WORKERS']

    owner = [
        'BUSINESS OWNERS',
        'BUSINESS'
        'BUSINESS -OWNERS',
        'BUSINESS OWNER',
        'BSNS-SHPS,CFT,PR',
        'SHOPOWNER/CRAFTSMEN',
        'OWNERS OF SHOPS OR CAMPANIES',
        'OWNERS SHOPS COMPANIES',
        'Business Owners',
        'BUSINESS PROPRIETORS',
        'OWNERS OF SHOPS',
        'SHOP OWNER, CRAFTSMEN']

    white_collar = [
        'WHITE COLLAR',
        'WHITE COLLAR -OF',
        'WHITE COL OF WKR',
        'WHITE COLLAR WORKER',
        'NON-OFFICE EMPLOYEES',
        'MIDDLE MANAGEMENT',
        'Other Office Employees',
        'OTHER OFFICE EMPLOYEES',
        'SUPERVISORS',
        'MIDDLE MANAGEMEN',
        'OTH OFFICE EMPLOY',
        'NON-OFFICE EMPLOY',
        'SUPERVISOR',
        'EMPLOYED SERVICE JOB',
        'OTHER OFFICE',
        'Middle Management',
        'Supervisors',
        'Non Office Employees',
        'NON OFFICE EMPLOYEES',
        'EMPLOYED AT A DESK',
        'EMPLOYED TRAVELLING']

    executive = [
        'EXECUTIVES',
        'EXECUTIVE',
        'EXECUTIVE,TOP MA',
        'EXECUTIVE TOP MANAG',
        'GENERAL MANAGEMENT',
        'GENERAL MANAGEME',
        'General Management']

    retired = ['RETIRED', 'RETITRED', 'Retired']

    housewife = [
        'HOUSEWIVES',
        'HOUSEWIFE',
        'HOUSEWIFE,NOT EM',
        'HSWF,NT OTHW EMP',
        'LOOKING AFTER HH',
        'HSWF NT OTHW EMP',
        'HOUSEWIFE NOT EMPL',
        'Housewife',
        'HOUSEWIFE NOT EMPLOYED']

    stud_mil = [
        'STUDENT/MILIT SERV',
        'STUDENT, MILIT SERV',
        'STUDENT,MILITARY',
        'STUDENT MILIT SERV',
        'Student',
        'MILITARY',
        'STUDENT',
        'Military Service',
        'MILITARY SERVICE']

    unemployed = ['UNEMPLOYED <DK NA>', 'UNEMPLOYMENT', 'UNEMPLOYED',
                  'TEMP UNEMPLOYED', 'Temp Unemployed', 'UNEMPLOYED TEMPORARLY']

    # HANDLE NAN CASE.
    occupation = x['OCCUPATION OF RESPONDENT']
    if occupation in farmer:
        return 1
    elif occupation in professional:
        return 2
    elif occupation in manual:
        return 3
    elif occupation in owner:
        return 4
    elif occupation in white_collar:
        return 5
    elif occupation in executive:
        return 6
    elif occupation in retired:
        return 7
    elif occupation in housewife:
        return 8
    elif occupation in stud_mil:
        return 9
    elif occupation in unemployed:
        return 10
    else:
        return numpy.NAN

if __name__ == '__main__':
    main()
