#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>

import pandas


def main():
    df = pandas.DataFrame()
    # Get two dataframes with matching mix of categorical and non-categorical
    # data.

    ZA0994 = pandas.read_pickle('reproduction_data/per_survey/ZA0994.pickle')
    print 'ZA0994:', len(ZA0994.index)
    ZA1319 = pandas.read_pickle('reproduction_data/per_survey/ZA1319.pickle')
    print 'ZA1319:', len(ZA1319.index)
    # Get List of Columns.
    columns = ZA0994.columns

    for col in columns:
        if ZA0994[col].dtype is 'category':
            df[col] = pandas.concat([ZA1319[col], ZA0994[col]]).astype('category')
        else:
            df[col] = pandas.concat([ZA1319[col], ZA0994[col]])

    print len(df.index)

if __name__ == '__main__':
    main()
