#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import numpy
import pandas


def main():
    data = pandas.read_pickle('reproduction_data/all_data.pickle')

    # print ZA1206['INCOME QUARTILES'].apply(fix_quartile)
    # print ZA1206['INCOME QUARTILES'].transform(lambda x: fix_quartile(x))
    ZA1206_quartiles = data[
        data['Study Number'] == 'ZA1206']['INCOME QUARTILES'].transform(
        lambda x: fix_quartile(x))

    print ZA1206_quartiles.head()
    print data[data['Study Number'] == 'ZA1206']['INCOME QUARTILES'].head()
    data.loc[data['Study Number'] == 'ZA1206', 'iquartile'] = ZA1206_quartiles
    print data.loc[data['Study Number'] == 'ZA1206', 'iquartile'].head()


def fix_quartile(x):
    if x == 'LOW':
        return 0
    if x == 2:
        return 1
    if x == 3:
        return 2
    if x == 'HIGH':
        return 3
    else:
        return numpy.nan


if __name__ == '__main__':
    main()
