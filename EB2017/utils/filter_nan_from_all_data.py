#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import pandas


def main():
    df = pandas.read_pickle('reproduction_data/all_data.pickle')

    c = [u'AGE EDUCATION',
         u'AGE EXACT',
         u'INCOME',
         u'OCCUPATION OF RESPONDENT',
         u'POL DISCUSSION - FREQUENCY',
         u'SEX',
         u'VALUE ORIENTATION - POL GOALS 1ST',
         u'VALUE ORIENTATION - POL GOALS 2ND',
         u'VOTE INTENTION']

    # Drop where any of the columns in c are nan:
    df = df.dropna(subset=c)

    print 'Writing File:'
    pickle_survey(df, 'all_data')


def pickle_survey(data, filename):
    # Write the required data in more useful format.
    outfile = 'reproduction_data/' + filename + '.pickle'
    data.to_pickle(outfile)
    print outfile


if __name__ == '__main__':
    main()
