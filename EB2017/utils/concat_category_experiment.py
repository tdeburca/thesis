#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@ehealthnigeria.org>

import pandas


def main():
    # Create Empty Dataframe.
    # print "Creating Empty Set..."
    df = pandas.DataFrame()

    # Import Survey/Variable List as dataframe.
    # survey = pandas.read_csv("smalltest.csv")
    print "Reading Data..."
    survey = pandas.read_csv("EurobarometerData78-92.csv")
    # Extract list of Surveys
    surveys = survey.columns.drop('Study Number')
    column_names = survey['Study Number'].tolist()

    for eb in surveys:
        print eb,
        filename, non_categorical, categorical, eb_metadata = parse_survey(
            column_names, survey, eb)
        cat_data = get_cat_data(filename, categorical)
        non_cat_data = get_non_cat_data(filename, non_categorical)
        data = pandas.concat([cat_data, non_cat_data], axis=1)
        # EB Data needs to be added to every row of data.
        # Must include eb, YEAR, MONTH
        data['YEAR'] = eb_metadata['YEAR']
        data['MONTH'] = eb_metadata['MONTH']
        data['Study Number'] = eb

        if len(df.index) == 0:
            df = data
        else:
            columns = data.columns
            for col in columns:
                if data[col].dtype is 'category':
                    print data[col].cat.categories
                    # df[col] = pandas.concat([df[col], data[col]]).astype('category')
                    pandas.concat([df[col], data[col]]).astype('category')
                else:
                    # df[col] = pandas.concat([df[col], data[col]])
                    pandas.concat([df[col], data[col]])

        print len(df.index)

    print 'Writing File:'
    pickle_survey(df, 'all_data')


def get_non_cat_data(filename, non_categorical):
    bad_keys = []
    for i in non_categorical:
        if str(non_categorical[i]) == 'nan':
            bad_keys.append(i)
    for key in bad_keys:
        non_categorical.pop(key)
    data = pandas.read_stata(
        filename,
        columns=non_categorical.values(),
        convert_categoricals=False)
    data.columns = non_categorical.keys()
    return data


def get_cat_data(filename, categorical):
    bad_keys = []
    for i in categorical:
        if str(categorical[i]) == 'nan':
            bad_keys.append(i)
    for key in bad_keys:
        categorical.pop(key)
    data = pandas.read_stata(filename, columns=categorical.values())
    data.columns = categorical.keys()
    return data


def parse_survey(column_names, survey, eb):
    # Populate dict let us pick vars by name.
    var_list = survey[eb].tolist()
    get_var = dict(zip(column_names, var_list))
    # Get input filename.
    filename = 'eb78-92/' + get_var.pop('Filename')
    # Two types of data, categorical, non-categorical.
    # See : https://gitlab.com/tdeburca/thesis/issues/1
    non_categorical = {}
    non_categorical['VOTE INTENTION'] = get_var.pop('VOTE INTENTION')
    non_categorical['INCOME'] = get_var.pop('INCOME')
    non_categorical['REGION'] = get_var.pop('REGION')
    # Non-eb vars (Metadata about the survey: year, month,
    eb_metadata = {}
    eb_metadata['YEAR'] = get_var.pop('YEAR')
    eb_metadata['MONTH'] = get_var.pop('MONTH')
    categorical = get_var
    # Add iso country code to list of data to be retrieved)
    categorical['isocntry'] = 'isocntry'
    return filename, non_categorical, categorical, eb_metadata


def pickle_survey(data, filename):
    # Write the required data in more useful format.
    outfile = 'reproduction_data/' + filename + '.pickle'
    data.to_pickle(outfile)
    print outfile

if __name__ == '__main__':
    main()
