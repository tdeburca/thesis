#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>

import pandas
pandas.set_option('chained_assignment', None)
from tqdm import tqdm
tqdm.pandas()


def main():
    data = pandas.read_pickle('reproduction_data/all_data.pickle')
    Explanatory_Vars = pandas.DataFrame()
    # Think about cases where quartile data unavailable.
    inc_lvl = data[['INCOME QUARTILES', 'INCOME']]
    # edu_lvl.progress_apply(low_ed, axis=1)
    Explanatory_Vars['INC_LOW'] = inc_lvl.progress_apply(low_inc, axis=1)
    Explanatory_Vars['INC_LOW_MID'] = inc_lvl.progress_apply(
        low_mid_inc, axis=1)
    Explanatory_Vars['INC_HIGH_MID'] = inc_lvl.progress_apply(
        high_mid_inc, axis=1)
    Explanatory_Vars['INC_HIGH'] = inc_lvl.progress_apply(high_inc, axis=1)

    print Explanatory_Vars['INC_LOW'].sum()
    print Explanatory_Vars['INC_LOW_MID'].sum()
    print Explanatory_Vars['INC_HIGH_MID'].sum()
    print Explanatory_Vars['INC_HIGH'].sum()


def low_inc(x):
    Low = [
        '- -',
        'LOW',
        'LOWEST QUARTILE',
        'LOWEST',
        '<LOWEST QUARTILE>',
        'POOREST']
    if x['INCOME QUARTILES'] in Low:
        return 1


def low_mid_inc(x):
    Low_Mid = [
        '2',
        '-',
        'NEXT TO LOWEST',
        '<NEXT TO LOWEST>',
        'NEXT TO POOREST',
        'NEXT TO LOW']
    if x['INCOME QUARTILES'] in Low_Mid:
        return 1


def high_mid_inc(x):
    High_Mid = [
        '.',  # ZA1208 Appears to encode High Mid quartile as '.'.
        '3',
        '+',
        'NEXT TO HIGHEST',
        '<NEXT TO HIGHEST>',
        'NEXT TO RICHEST',
        'NEXT TO HIGH']
    if x['INCOME QUARTILES'] in High_Mid:
        return 1


def high_inc(x):
    High = ['+ +', 'HIGH', 'HIGHEST QUARTILE', '<HIGHEST QUARTILE>', 'RICHEST']
    if x['INCOME QUARTILES'] in High:
        return 1


if __name__ == '__main__':
    main()
