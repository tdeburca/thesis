#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>

import pandas
from iso3166 import countries
# Give very basic progress bars.
from tqdm import tqdm
tqdm.pandas()
'''
Create three files from dataframes:
Explanatory_Vars.pickle
Dependent_Vars.pickle
Control_Vars.pickle
'''

# Disable warning:
pandas.set_option('chained_assignment', None)
'''
This disables this warning:
prep_explanatory_vars.py:58: SettingWithCopyWarning:
A value is trying to be set on a copy of a slice from a DataFrame.
Try using .loc[row_indexer,col_indexer] = value instead
--
See 'TODO:' below.

'''


def main():
    # Import Question Set.
    print 'Loading Data...'
    eb_data = pandas.read_pickle('reproduction_data/all_data.pickle')
    dpi_data = get_dpi_data(eb_data)
    dpi_data.to_pickle('dpi_data.pickle')


def alpha_only(name_list):
        # Normalize country names:
    temp_list = []
    for name in name_list:
        alpha_only_string = ''.join([i for i in name if i.isalpha()])
        temp_list.append(alpha_only_string.upper())
    return temp_list


def get_dpi_data(eb_data):
    dpi_data = pandas.read_stata('DPI/DPI2015/DPI2015.dta')
    # Set date index on dpi_data
    dpi_data = dpi_data.set_index(pandas.DatetimeIndex(dpi_data['year']))
    # Limit to years we care about
    dpi_start = eb_data.YEAR.min() + '-01-01'
    dpi_end = eb_data.YEAR.max() + '-01-01'
    # Filter Data
    dpi_data = dpi_data.loc[dpi_start: dpi_end]
    # Limit to only EU Countries
    eu_country_set = get_eu_countries(eb_data)
    dpi_data = get_dpi_subset(dpi_data, eu_country_set)
    # The DPI uses 'KF' to denote the Danish Conservative party,
    # and SD to denote the Social Democrats. This is
    # amibguous and so is replaced with 'CONSERVATIVE' for clarity.
    dpi_data.gov1me.replace(
        to_replace='KF', value='CONSERVATIVE', inplace=True)
    dpi_data.gov1me.replace(
        to_replace='SD', value='SOCIALDEMOCRAT', inplace=True)
    # The DPI encoding for belgium is CSP which matches onto the Flemish and
    # French Christian Socialists. After some testing this string matches
    # well.
    dpi_data.gov1me.replace(to_replace='CSP', value='CHRISTIAN', inplace=True)
    # The DPI data describes the spanish government from 87-92 as PSOE/PSC.
    # The PSC (Parit Socialista de Catalunya-Congres) don't appear in the EB
    # dataset so reclassifing as PSOE.
    dpi_data.gov1me.replace(to_replace='PSOE/PSC', value='PSOE', inplace=True)
    # POSL doesn't appear in EB data, SLAP, or SOCIALIST PARTY is how they're
    # coded.
    dpi_data.gov1me.replace(
        to_replace='POSL',
        value='SOCIALIST PARTY',
        inplace=True)
    # PCS doesn't appear in EB data, maps to CSV.
    dpi_data.gov1me.replace(to_replace='PCS', value='CSV', inplace=True)
    # ND is short for New Democracy, which is how it's referenced in EB data.
    dpi_data.gov1me.replace(
        to_replace='ND',
        value='NEW DEMOCRACY',
        inplace=True)
    # Portuguese Socialists (PSP) are in the eb dataset as : 'PORTUGUESE
    # SOCIALIST PARTY (PS)'
    dpi_data.gov1me.replace(
        to_replace='PSP',
        value='SOCIALIST PARTY',
        inplace=True)

    return dpi_data


def get_dpi_subset(dpi_data, country_set):
    return dpi_data[dpi_data['countryname'].isin(country_set)]


def get_eu_countries(eb_data):
    # Get list of countries we care about
    eu_apolitical_names = set()
    for i in eb_data.isocntry.unique():
        eu_apolitical_names.add(get_apol_name(i))
    return eu_apolitical_names


def get_apol_name(isocntry):
    # This information may code incorrectly for northern Ireland.
    # Their 'rulling party' will never match 'Conservative'/'Labour'
    # Should i match on parties containing labour and parties containing
    # 'Unionist'?
    if isocntry in ['GB-GBN', 'GB', 'GB-NIR', 'GB_GBN', 'GB_NIR']:
        countryname = 'UK'
    elif isocntry in ['DE-W', 'DE']:
        countryname = 'FRG/Germany'
    elif isocntry == 'DE-E':
        countryname = 'GDR'
    else:
        countryname = countries.get(isocntry).apolitical_name
    return countryname


if __name__ == '__main__':
    main()
