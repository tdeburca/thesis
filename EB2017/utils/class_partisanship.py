#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright © 2017-06-13 Tiarnan de Burca <tdeburca@gmail.com>

import pandas
import pickle
# Give very basic progress bars.
from tqdm import tqdm
tqdm.pandas()
pandas.set_option('chained_assignment', None)


def main():
    Explanatory_Vars = pandas.DataFrame()
    print 'Loading Data...'
    data = pandas.read_pickle('reproduction_data/all_data.pickle')
    cp_lookup = pickle.load(open('pickles/cp.pickle', 'r'))

    # Class Partisanship
    vi_data = pickle.load(open('pickles/VoterIntention.pickle'))
    gov_lookup = pickle.load(open('pickles/gov_lookup.pickle'))
    CP = data[
        ['VOTE INTENTION', 'YEAR', 'isocntry', 'Study Number']]
    Explanatory_Vars['CP Prol'] = CP.progress_apply(
        class_partisanship_prol, args=(vi_data, cp_lookup), axis=1)
    Explanatory_Vars['CP Bour'] = CP.progress_apply(
        class_partisanship_bour, args=(vi_data, cp_lookup), axis=1)

    print Explanatory_Vars.head(10)
    print Explanatory_Vars['CP Prol'].sum()
    print Explanatory_Vars['CP Bour'].sum()


def class_partisanship_prol(x, vi_data, cp_lookup):
    p = 0
    prol = cp_lookup['prol']
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # During the period under the study the people of Northern Ireland didn't
    # vote for parties that ruled the UK. Omit GB-NIR.
    if x.isocntry == 'GB-NIR':
        return 0
    # No East German Parties are defined as Prol or Bour parties. Omitting.
    elif x.isocntry == 'DE-E':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    if preferred_party in prol[x['Study Number']][vi_country]:
        print 'prol', x['Study Number'], [vi_country], preferred_party 
        p = 1
    return p

def class_partisanship_bour(x, vi_data, cp_lookup):
    b = 0
    bour = cp_lookup['bour']
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # During the period under the study the people of Northern Ireland didn't
    # vote for parties that ruled the UK. Omit GB-NIR.
    if x.isocntry == 'GB-NIR':
        return 0
    # No East German Parties are defined as Prol or Bour parties. Omitting.
    elif x.isocntry == 'DE-E':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    if vi_country in bour[x['Study Number']]:
        if preferred_party in bour[x['Study Number']][vi_country]:
            print 'bour', x['Study Number'], [vi_country], preferred_party 
            b = 1
    return b

                                                                                 
if __name__ == '__main__':                                                       
    main()  
