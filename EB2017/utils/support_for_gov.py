#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>

import pandas
import pickle
# Give very basic progress bars.
from tqdm import tqdm
tqdm.pandas()

# Disable warning:
pandas.set_option('chained_assignment', None)
'''
This disables this warning:
prep_explanatory_vars.py:58: SettingWithCopyWarning:
A value is trying to be set on a copy of a slice from a DataFrame.
Try using .loc[row_indexer,col_indexer] = value instead
--
See 'TODO:' below.

'''


def main():
    # Import Question Set.
    print 'Loading Data...'
    eb_data = pandas.read_pickle('reproduction_data/all_data.pickle')
    vi_data = pickle.load(open('pickles/VoterIntention.pickle'))
    gov_lookup = pickle.load(open('pickles/gov_lookup.pickle'))
    # COUNTRY var required for studies where isocntry considers GB&NI as one.
    VI = eb_data[
        ['VOTE INTENTION', 'YEAR', 'isocntry', 'Study Number', 'COUNTRY']]
    # Only first 1000 rows.
    # VI = VI.head(1000)
    # VI = VI[VI.isocntry == 'IE']
    # VI = VI.COUNTRY.dropna()
    # VI = VI[VI['Study Number'] =='ZA1209']
    print '--'
    parties = VI.progress_apply(
        support_for_gov,
        args=(vi_data, gov_lookup), axis=1)
    print parties


def support_for_gov(x, vi_data, gov_lookup):
    support = 0
    # If respondant didn't answer the question, return NaN.
    if pandas.isnull(x['VOTE INTENTION']):
        return 0
    vi_code = str(int(x['VOTE INTENTION']))
    # All codes 90 and greater are DK/Blank Ballot/Other Party so return.
    if int(vi_code) > 89:
        return 0
    # In studiees ZA1209/1752/1753 GB is coded as a single entity without a
    # breakout for Northern Ireland, for these studies we import a separate
    # variable and code based on it.
    # During the period under the study the people of Northern Ireland
    if x.isocntry == 'GB-NIR':
        return 0
    elif x.isocntry == 'GB':
        vi_country = 'GB-GBN'
    else:
        vi_country = x.isocntry
    # Some data is miscoded, i.e. coded as something for which there is no
    # valid answer, for this I return same as a failure to answer the question.
    if vi_code not in vi_data[x['Study Number']][vi_country].keys():
        return 0
    preferred_party = vi_data[x['Study Number']][vi_country][vi_code]
    # print x.YEAR, x['Study Number'], vi_country
    a = gov_lookup[x.YEAR][x['Study Number']][vi_country]['eb-match']
    # To make comparison possible, normalize strings. (Only Alpha, all-caps.)
    preferred_party = alpha_only([preferred_party])
    gov_party = alpha_only([a])
    if gov_party[0] in preferred_party[0]:
        support = 1
    return support


def alpha_only(name_list):
    # Normalize country names:
    temp_list = []
    for name in name_list:
        alpha_only_string = ''.join([i for i in name if i.isalpha()])
        temp_list.append(alpha_only_string.upper())
    return temp_list

if __name__ == '__main__':
    main()
