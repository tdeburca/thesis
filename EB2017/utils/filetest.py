#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@ehealthnigeria.org>

import pandas


def main():
    # Import Survey/Variable List as dataframe.
    # survey = pandas.read_csv("smalltest.csv")
    survey = pandas.read_csv("EurobarometerData78-92.csv")

    # Extract list of Surveys
    surveys = survey.columns.drop('Study Number')
    columnnames = survey['Study Number'].tolist()
    columnnames.pop(0)  # Remove 'Filename'

    # ZA0994 = survey[['Study Number','ZA0994']]
    for i in surveys:
        # Open the file.
        filename = 'eb78-92/' + survey[i].pop(0)
        var_list = survey[i].dropna().tolist()
        for field in var_list:
            try:
                pandas.read_stata(
                    filename,
                    columns=['isocntry'])
            except Exception as e:
                print 'Error Reading:', filename, 'Field:', field
                print(e)


if __name__ == '__main__':
    main()
