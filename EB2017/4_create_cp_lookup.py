#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>
import pickle
prol = {'FR': ['Communist Party', 'Socialist Party', 'Lutte Ouvriere'],
        # Wallon , Flemish check.
        'BE': ['Communist Party', 'Socialist Party'],
        'NL': ['CPN', 'PVDA'],
        'DE-W': ['Social Democrat'],
        'IT': ['PCI', 'PSI'],
        'LU': ['KP', 'LSAP'],
        'DK': ['Communist Party', 'Social Democratic', 'Socialist People', 'Left Socialist'],
        'IE': ['Labour', 'Workers'],
        'GB-GBN': ['Labour'],
        'GR': ['KKE', 'KKE international', 'PA.SO.K'],
        'ES': ['PCI', 'PSOE'],
        'PT': ['COMMUNIST PARTY', 'SOCIALIST PARTY', 'DEMOCRATIC PEOPLE', 'CDU', 'UDP']}
bour = {'FR': ['RPR', 'UDF'],
        # Walloon Flemish Check.
        'BE': ['PRL', 'Liberal'],
        'NL': ['CDA',  'VVD'],
        'DE-W': ['CDU', 'CSU', 'FDP'],
        'IT': ['CD', 'PLI'],
        'DK': ['CONSERVATIVE'],
        'GB-GBN': ['Conservative'],
        'ES': ['COMMUNIST','UNITED LEFT'],
        'PT': ['PDC', 'CDS']
        }
vi_data = pickle.load(open('pickles/VoterIntention.pickle'))

def main():
    # Desired Outcome: 
    # List of parties, by coutntry, as coded by EB, which represent Prol or bourgeois alignment. 
    # survey[prol][country][list]
    prol_eb = {}
    bour_eb = {}
    print 'Get prol Parties'
    for survey in vi_data.keys():
        prol_eb[survey] = check_prol_survey(survey, vi_data)
    print 'Get bour Parties'
    for survey in vi_data.keys():
        bour_eb[survey] = check_bour_survey(survey, vi_data)

    print 'Write File'
    cp_dict ={'bour' : bour_eb, 'prol' : prol_eb}
    outfile = 'pickles/cp.pickle'
    pickle.dump(cp_dict, open(outfile, 'wb'))



def check_bour_survey(survey, vi_data):
    country_list = vi_data[survey].keys()
    # No Ulster parties listed in Prol/Bour lists. Safe to omit.  
    if 'GB-NIR' in country_list:
        country_list.remove('GB-NIR')
    # No East German parties are included in the survey. Safe to Omit.
    if 'DE-E' in country_list:
        country_list.remove('DE-E')
    # I was not able to identify a bourgeois party in Luxembourg or Ireland.
    if 'LU' in country_list:
        country_list.remove('LU')
    if 'IE' in country_list:
        country_list.remove('IE')
    # While the author makes no reference to being unable to find a bourgeois party in Greece, none is provided.
    if 'GR' in country_list:
        country_list.remove('GR')
    match_dict = {}
    for country in country_list:
        match, matches = check_bour_match(survey, country)
        if  match == 0:
            print 'Fail:', survey, bour[country], vi_data[survey][country]
        else:
            match_dict[country] = matches
    return match_dict

def check_bour_match(survey, country):
    match = 0
    matches = []
    vi_parties = get_vi_parties(survey, country)
    bour_parties = get_bour_parties(country)
    for bour_party in bour_parties:
        bour_party = alpha_only([bour_party])[0]
        for vi_party in vi_parties:
            vi_party_clean = alpha_only([vi_party])[0]
            if bour_party in vi_party_clean:
                match += 1
                matches.append(vi_party)
    return match, matches

def get_bour_parties(country):
    return bour[country]

def check_prol_survey(survey, vi_data):
    country_list = vi_data[survey].keys()
    # No Ulster parties listed in Prol/Bour lists. Safe to omit.  
    if 'GB-NIR' in country_list:
        country_list.remove('GB-NIR')
    # No East German parties are included in the survey. Safe to Omit.
    if 'DE-E' in country_list:
        country_list.remove('DE-E')
    match_dict = {}
    for country in country_list:
        match, matches = check_prol_match(survey, country)
        if match == 0:
            print 'Fail:', survey, prol[country], vi_data[survey][country]
        else:
            match_dict[country] = matches
    return match_dict

def check_prol_match(survey, country):
    match = 0
    matches = []
    vi_parties = get_vi_parties(survey, country)
    prol_parties = get_prol_parties(country)
    for prol_party in prol_parties:
        prol_party = alpha_only([prol_party])[0]
        for vi_party in vi_parties:
            vi_party_clean = alpha_only([vi_party])[0]
            # print prol_party, vi_party
            if prol_party in vi_party_clean:
                match += 1
                matches.append(vi_party)
    return match, matches

def get_prol_parties(country):
    return prol[country]

def get_vi_parties(survey, country):
    try:
        return vi_data[survey][country].values()
    except:
        return []


def party_list_compare(pb_list, vi_list):
    match = 0
    for pb in alpha_only(pb_list):
        for party in alpha_only(vi_list):
            # print pb, party
            if pb in party:
               match += 1
    return match


def alpha_only(name_list):
    # Normalize country names:
    temp_list = []
    for name in name_list:
        alpha_only_string = ''.join([i for i in name if i.isalpha()])
        temp_list.append(alpha_only_string.upper())
    return temp_list


if __name__ == '__main__':
    main()
