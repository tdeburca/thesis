#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright © 2017-06-02 Tiarnan de Burca <tdeburca@gmail.com>
import pandas
import pickle
from iso3166 import countries


def main():
    # Get studies for year.
    # Import Question Set.
    print 'Loading Data...'
    eb_data = pandas.read_pickle('reproduction_data/Model_1.pickle')
    vi_data = pickle.load(open('pickles/VoterIntention.pickle'))
    dpi_data = get_dpi_data(eb_data)

    eb_lookup = {'1978': ['ZA0994'],
                 '1979': ['ZA1036', 'ZA1037'],
                 '1980': ['ZA1038', 'ZA1039'],
                 '1981': ['ZA1206', 'ZA1207'],
                 '1982': ['ZA1208', 'ZA1209'],
                 '1983': ['ZA1318', 'ZA1319'],
                 '1984': ['ZA1320', 'ZA1321'],
                 '1985': ['ZA1541', 'ZA1542'],
                 '1986': ['ZA1543', 'ZA1544'],
                 '1987': ['ZA1712', 'ZA1713'],
                 '1988': ['ZA1714', 'ZA1715'],
                 '1989': ['ZA1750', 'ZA1752'],
                 '1990': ['ZA1753', 'ZA1960'],
                 '1991': ['ZA2031', 'ZA2081'],
                 '1992': ['ZA2141']}
    # Code only compares largest government party. Needs Reveiw : Number of
    # GOV parties kept here: (DPI) GOVOTH
    # Generate a dictionary that allows lookup of ruling party in terms of EB
    # Dataset. Add france afterwards for simplicity.
    gov_lookup = {}
    for year in sorted(eb_lookup.keys()):
        this_years_winners = dpi_data[(dpi_data.index.year == int(year))]
        gov_lookup[year] = {}
        for study in eb_lookup[str(year)]:
            print year, study
            gov_lookup[year][study] = {}
            country_list = eb_data[eb_data['Study Number']
                                   == study].isocntry.unique().tolist()
            print country_list
            # 'Rulling Party' information for Northern Ireland doesn't make sense in its case, removed from dataset.
            if 'GB-NIR' in country_list:
                country_list.remove('GB-NIR')
            for country in country_list:
                match = False
                # print country,
                country_political = get_apol_name(country)
                if country == 'GB':
                    country = 'GB-GBN'
                winner = list(
                    this_years_winners
                    [this_years_winners['countryname'] == country_political]
                    ['gov1me'])[0]
                for party in vi_data[study][country].values():
                    if alpha_only([winner])[0] in alpha_only([party])[0]:
                        match = True
                        matchers = [winner, party]
                if match:
                    gov_lookup[year][study][country] = {
                        'dpi-winner': matchers[0], 'eb-match': matchers[1]}
                    print 'Match: ', country, matchers
                else:
                    gov_lookup[year][study][country] = {
                        'dpi-winner': winner, 'eb-match': 'FIXME'}
                    print 'Fail : ', country, winner
    # There are a couple of special cases not covered by the dpi-data directly.
    # In the case of france we want presidential information, not prime
    # minister, and in the case of 1990 east germany we wish to correct the
    # information in the database which returns 'National Front' (Possibly as an
    # artifact of war and reunification.) The correct answer is the CDU.
    # FIXME:  Review handling of East germany, particularly after 1990
    for year in gov_lookup.keys():
        for study in gov_lookup[year].keys():
            for country in gov_lookup[year][study].keys():
                if gov_lookup[year][study][country]['eb-match'] == 'FIXME':
                    if (country == 'FR') and (1978 <= year <= 1980):
                        gov_lookup[year][study][country][
                            'eb-match'] = 'REPUBLIC'
                    if (country == 'FR') and (1980 < year):
                        gov_lookup[year][study][country][
                            'eb-match'] = 'SOCIALIST'
                    if (country == 'DE-E'):
                        gov_lookup[year][study][country][
                            'eb-match'] = 'CDU'

    outfile = 'pickles/gov_lookup.pickle'
    print 'Output to file', outfile
    pickle.dump(gov_lookup, open(outfile, 'wb'))


def get_dpi_data(eb_data):
    dpi_data = pandas.read_stata('DPI/DPI2015/DPI2015.dta')
    # Set date index on dpi_data
    dpi_data = dpi_data.set_index(pandas.DatetimeIndex(dpi_data['year']))
    # Limit to years we care about
    dpi_start = str(eb_data.YEAR.min()) + '-01-01'
    dpi_end = str(eb_data.YEAR.max()) + '-01-01'
    # Filter Data
    dpi_data = dpi_data.loc[dpi_start: dpi_end]
    # Limit to only EU Countries
    eu_country_set = get_eu_countries(eb_data)
    dpi_data = get_dpi_subset(dpi_data, eu_country_set)
    # The DPI uses 'KF' to denote the Danish Conservative party,
    # and SD to denote the Social Democrats. This is
    # amibguous and so is replaced with 'CONSERVATIVE' for clarity.
    dpi_data.gov1me.replace(
        to_replace='KF', value='CONSERVATIVE', inplace=True)
    dpi_data.gov1me.replace(
        to_replace='SD', value='SOCIALDEMOCRAT', inplace=True)
    # The DPI encoding for belgium is CSP which matches onto the Flemish and
    # French Christian Socialists. After some testing this string matches
    # well.
    dpi_data.gov1me.replace(to_replace='CSP', value='CHRISTIAN', inplace=True)
    # The DPI data describes the spanish government from 87-92 as PSOE/PSC.
    # The PSC (Parit Socialista de Catalunya-Congres) don't appear in the EB
    # dataset so reclassifing as PSOE.
    dpi_data.gov1me.replace(to_replace='PSOE/PSC', value='PSOE', inplace=True)
    # POSL doesn't appear in EB data, SLAP, or SOCIALIST PARTY is how they're
    # coded.
    dpi_data.gov1me.replace(
        to_replace='POSL',
        value='SOCIALIST PARTY',
        inplace=True)
    # PCS doesn't appear in EB data, maps to CSV.
    dpi_data.gov1me.replace(to_replace='PCS', value='CSV', inplace=True)
    # ND is short for New Democracy, which is how it's referenced in EB data.
    dpi_data.gov1me.replace(
        to_replace='ND',
        value='NEW DEMOCRACY',
        inplace=True)
    # Portuguese Socialists (PSP) are in the eb dataset as : 'PORTUGUESE
    # SOCIALIST PARTY (PS)'
    dpi_data.gov1me.replace(
        to_replace='PSP',
        value='SOCIALIST PARTY',
        inplace=True)

    return dpi_data


def get_dpi_subset(dpi_data, country_set):
    return dpi_data[dpi_data['countryname'].isin(country_set)]


def get_eu_countries(eb_data):
    # Get list of countries we care about
    eu_apolitical_names = set()
    for i in eb_data.isocntry.unique():
        eu_apolitical_names.add(get_apol_name(i))
    return eu_apolitical_names


def get_apol_name(isocntry):
    # This information may code incorrectly for northern Ireland.
    # Their 'rulling party' will never match 'Conservative'/'Labour'
    # Should i match on parties containing labour and parties containing
    # 'Unionist'?
    if isocntry in ['GB-GBN', 'GB', 'GB-NIR', 'GB_GBN', 'GB_NIR']:
        countryname = 'UK'
    elif isocntry in ['DE-W', 'DE']:
        countryname = 'FRG/Germany'
    elif isocntry == 'DE-E':
        countryname = 'GDR'
    else:
        countryname = countries.get(isocntry).apolitical_name
    return countryname


def alpha_only(name_list):
    # Normalize country names:
    temp_list = []
    for name in name_list:
        alpha_only_string = ''.join([i for i in name if i.isalpha()])
        temp_list.append(alpha_only_string.upper())
    return temp_list

if __name__ == '__main__':
    main()
