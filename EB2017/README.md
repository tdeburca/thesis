Requirements:
This code was run and tested with the following library versions, it is likely to work with newer versions, but tested versions are shown here. 

* pandas==0.20.1
* statsmodels==0.8.0
* scikit-learn==0.18.1


To reproduce the EB 2017 model run the files in numerical order:

Take data contained in the eb78-92 directory and convert into desired format.
* 1_import_all_data.py 

Create tables to allow measurement of  'Support For Government' and 'Class Partisanship'.
* 2_create_vi_data.py
* 3_create_gov_lookup.py
* 4_create_cp_lookup.py

Convert data into the format required to run the regression. (Dummys etc.)
* 5_prep_regression_vars.py

Run Regressions, output results. 
* 6_regressios_mc.py



