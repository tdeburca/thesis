#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@ehealthnigeria.org>

import pandas
import numpy


def main():
    # Create Empty Dataframe.
    print "Creating Empty Set..."
    df = pandas.DataFrame()

    # Import Survey/Variable List as dataframe.
    print "Reading Data..."
    survey = pandas.read_csv("EurobarometerData78-92.csv")

    # Extract list of Surveys
    surveys = survey.columns.drop('Study Number')
    column_names = survey['Study Number'].tolist()

    for eb in surveys:
        print eb,
        filename, non_categorical, categorical, eb_metadata = parse_survey(
            column_names, survey, eb)
        cat_data = get_cat_data(filename, categorical)
        print 'categorial, done.'
        non_cat_data = get_non_cat_data(filename, non_categorical)
        print 'non-categorial, done.'
        data = pandas.concat([cat_data, non_cat_data], axis=1)
        # To get income quartile data, income data is put in quartile buckets,
        # per-survey, per-country, and added to the column 'iquartile'.
        # Study ZA1206 doesn't provide an income variable so will have to be
        # handled elsewhere.
        try:
            data['iquartile'] = data.groupby('isocntry').INCOME.transform(
                lambda x: pandas.qcut(
                    x,
                    4,
                    labels=False,
                    duplicates='drop'))
        except AttributeError:
            data['iquartile'] = numpy.nan

        # EB Data needs to be added to every row of data.
        # Must include eb, YEAR, MONTH
        data['YEAR'] = eb_metadata['YEAR']
        data['MONTH'] = eb_metadata['MONTH']
        data['Study Number'] = eb
        df = pandas.concat([df, data])

    # Beween ZA0994 and ZA1753 West Germany is referred to as 'DE', to
    # standardize with later data, we change this to DE-W.
    df.isocntry = df.isocntry.replace(['DE'], 'DE-W')

    # ZA1206 doesn't provide an income variable, but does provide quartile data,
    # we remap this to our values and update the iquartile field in that survey.
    # Get quartile data, and rescale.
    ZA1206_quartiles = df[
        df['Study Number'] == 'ZA1206']['INCOME QUARTILES'].transform(
        lambda x: fix_quartile(x))
    # Put quartile data in the correct column.
    df.loc[df['Study Number'] == 'ZA1206', 'iquartile'] = ZA1206_quartiles
    # Drop Income and Income Quartile column.
    del df['INCOME QUARTILES']
    del df['INCOME']

    # European Weights need to be rescaled so that they map to 1 for all
    # surveys.
    for study in df['Study Number'].unique():
        weights = df[df['Study Number'] == study]['EUROPEAN WEIGHT']
        sum_of_weights = weights.sum()
        number_of_weights = weights.shape[0]
        coeff = number_of_weights / sum_of_weights
        df.loc[
            df['Study Number'] == study, 'EUROPEAN WEIGHT'] = df.loc[
            df['Study Number'] == study, 'EUROPEAN WEIGHT'] * coeff

    # Norway is included in some surveys but is beyond scope so needs to be
    # removed.
    df = df[df.isocntry != 'NO']
    # East Germany is included in some surveys but is beyond scope so needs to
    # be removed.
    df = df[df.isocntry != 'DE-E']
    # Force type of Year to int.
    df['YEAR'] = df['YEAR'].astype(int)

    # From Page 339, Note #8: I exclude surveys of nations that were not yet
    # members in the year surveyed.
    # Remove Greece before 1/1/1981.
    boolean = df[(df['isocntry'] == 'GR') & (df['YEAR'] < 1981)]
    df = df.drop(boolean.index)
    # Remove Spain & Portugal before 1/1/1986.
    boolean = df[(df['isocntry'] == 'ES') & (df['YEAR'] < 1986)]
    df = df.drop(boolean.index)
    boolean = df[(df['isocntry'] == 'PT') & (df['YEAR'] < 1986)]
    df = df.drop(boolean.index)

    # Northern Ireland is not mentioned in the paper once. As there's no
    # information on what the author did with the data on Northern Ireland I'm
    # left to assume that he omitted it. (This is compounded by the fact that he
    # dodesn't include information on the Bourgeois or Prolitariat parties, and
    # that for most elections at the time the people of Northern Ireland were
    # not voting for parties which would rule in Westminster.
    df = df[df.isocntry != 'GB-NIR']

    # In 'ZA1209', 'ZA1752', 'ZA1753' Great Britain and Northern Ireland are not
    # encoded in isocntry, for these cases we must use the COUNTRY variable to
    # drop Norther Ireland data, and set the isocntry correctly.

    # As the COUNTRY variable is only collected for the problematic surveys we
    # can remove all entries where it is set to 'NORTHERN IRELAND'.
    df = df[df.COUNTRY != 'NORTHERN IRELAND']
    # Moving remaining miscoded data from 'GB' to 'GB-GBN' to match the rest of
    # the dataset.
    df.isocntry = df.isocntry.replace(['GB'], 'GB-GBN')

    # COUNTRY variable can now be dropped.
    del df['COUNTRY']

    print 'List of countries in Dataset.'
    print df.isocntry.unique()

    # The 5 models each require a different set of the data.
    # Model 1 includes the full sample of respondent 1978-1992.
    print 'Writing File:'
    pickle_survey(df, 'Model_1')

    # Models 2/3 are the founder members in different time periods.
    founders = ['FR', 'BE', 'NL', 'DE-W', 'IT', 'LU']
    founder_data = df.loc[df['isocntry'].isin(founders)]
    # Models 4/5 are the members who have joined since foundation, in different
    # time periods. (`~` inverts a pandas boolean, in this case 'is NOT in'.)
    new_member_data = df.loc[~df['isocntry'].isin(founders)]

    # Model 2, founders: 1978-86
    pickle_survey(founder_data.loc[founder_data['YEAR'] <= 1986], 'Model_2')
    # Model 3, founders: 1987-92
    pickle_survey(founder_data.loc[founder_data['YEAR'] >= 1987], 'Model_3')

    # Model 4, New Members 1978-86
    pickle_survey(
        new_member_data.loc[
            new_member_data['YEAR'] <= 1986],
        'Model_4')
    # Model 5, New Members 1987-92
    pickle_survey(
        new_member_data.loc[
            new_member_data['YEAR'] >= 1987],
        'Model_5')


def fix_quartile(x):
    if x == 'LOW':
        return 0
    if x == 2:
        return 1
    if x == 3:
        return 2
    if x == 'HIGH':
        return 3
    else:
        return numpy.nan


def get_non_cat_data(filename, non_categorical):
    bad_keys = []
    for i in non_categorical:
        if str(non_categorical[i]) == 'nan':
            bad_keys.append(i)
    for key in bad_keys:
        non_categorical.pop(key)
    data = pandas.read_stata(
        filename,
        columns=non_categorical.values(),
        convert_categoricals=False)
    data.columns = non_categorical.keys()
    return data


def get_cat_data(filename, categorical):
    bad_keys = []
    for i in categorical:
        if str(categorical[i]) == 'nan':
            bad_keys.append(i)
    for key in bad_keys:
        categorical.pop(key)
    data = pandas.read_stata(filename, columns=categorical.values())
    data.columns = categorical.keys()
    return data


def parse_survey(column_names, survey, eb):
    # Populate dict let us pick vars by name.
    var_list = survey[eb].tolist()
    get_var = dict(zip(column_names, var_list))
    # Get input filename.
    filename = 'eb78-92/' + get_var.pop('Filename')
    # Two types of data, categorical, non-categorical.
    # See : https://gitlab.com/tdeburca/thesis/issues/1
    non_categorical = {}
    non_categorical['VOTE INTENTION'] = get_var.pop('VOTE INTENTION')
    non_categorical['INCOME'] = get_var.pop('INCOME')
    # REGION was found to have a small impact but is costly to reproduce so
    # is dropped from study.
    # non_categorical['REGION'] = get_var.pop('REGION')
    # Non-eb vars (Metadata about the survey: year, month)
    eb_metadata = {}
    eb_metadata['YEAR'] = get_var.pop('YEAR')
    eb_metadata['MONTH'] = get_var.pop('MONTH')
    categorical = get_var
    # Add iso country code to list of data to be retrieved)
    categorical['isocntry'] = 'isocntry'
    return filename, non_categorical, categorical, eb_metadata


def pickle_survey(data, filename):
    # Write the required data in more useful format.
    outfile = 'reproduction_data/' + filename + '.pickle'
    data.to_pickle(outfile)
    print outfile

if __name__ == '__main__':
    main()
