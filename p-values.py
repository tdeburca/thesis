#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>
import pickle

eb2017_base = 'data/'
basefile = 'outfile/Model_{}_result.pickle'


def main():
    for x in ['data/', 'Mannheim/']:
        for i in range(1, 6):
            print x, i
            res = pickle.load(open(x + basefile.format(i)))
            print 'p-values greater than .01. Result suspicious.'
            print res.pvalues[res.pvalues > 0.01]


if __name__ == '__main__':
    main()
