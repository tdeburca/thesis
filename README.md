# Thesis

This is a repository of the work done to complete my thesis, "Does the replication crisis extend to International Relations? Testing an Empirical Test.
Same tests, new tools. Public Support for European Integration, a modern approach to five theories."

* Course : LG5000 Dissertation
* Student No.: 15212847

Instructions to run the models can be found in the EB2017 and Mannheim directories. 