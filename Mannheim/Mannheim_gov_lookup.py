#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import pandas
import pickle

    # France is a special case, see below.
nations = {
    2: 'Belgium',
    3: 'Netherlands',
    4: 'FRG/Germany',
    5: 'Italy',
    6: 'Luxembourg',
    7: 'Denmark',
    8: 'Ireland',
    9: 'UK',
    11: 'Greece',
    12: 'Spain',
    13: 'Portugal'}

years = [1978,
         1979,
         1980,
         1981,
         1982,
         1983,
         1984,
         1985,
         1986,
         1987,
         1988,
         1989,
         1990,
         1991,
         1992]

# Goal Code number of the rulling party for every country in every year.
# This is a list of all parties that have ever been in government in a
# country from the study. (One Exception 'FP', smallest danish governemnt
# party 83/84 not coded by EBI.
gov_lookup = {2: {'CSP': [400, 401],
                  'CVP': [401],
                  'FDF': [701, 702],
                  'PRL': [301, 305, 307],
                  'PS': [200],
                  'PSC': [400],
                  'PVV': [300],
                  'SP': [201],
                  'Socialist': [200, 201]},
              3: {'CDA': [400], 'D66': [301], 'PvdA': [200], 'VVD': [300]},
              4: {'CDU': [400], 'CSU': [400], 'FDP': [300], 'SPD': [200]},
              5: {'DC': [400],
                  'Liberals': [301],
                  'PLI': [301],
                  'PRI': [300],
                  'Social Dem.': [201],
                  'Socialists': [200]},
              6: {'DP': [300], 'PCS': [400], 'POSL': [200]},
              7: {'FP': [0], 'CD': [400], 'KF': [500], 'RV': [302], 'SD': [200], 'V': [300]},
              8: {'Fianna Fail': [500], 'Fine Gael': [400], 'Labour': [200], 'PDP': [300]},
              9: {'Conservative': [500], 'Labour': [200], 'Liberal': [301]},
              11: {'Diana': [401], 'ND': [400], 'PASOK': [200]},
              12: {'PSOE': [200], 'PSOE/PSC': [200], 'UCD': [302]},
              13: {'AD (PSD+CDS)': [300, 201], 'CDS': [201], 'PSD': [300], 'PSP': [200]}}


def main():
    dpi_data = pandas.read_stata('../DPI/DPI2015/DPI2015.dta')
    dpi_data = dpi_data.set_index(pandas.DatetimeIndex(dpi_data['year']))

    # Get List of All Parties that have been in Government in the time period.
    output = {}
    for year in years:
        output[year] = {}
        winners = dpi_data[(dpi_data.index.year == year)]
        for nation in nations:
            gov_parties = []
            for govmem in ['gov1me', 'gov2me', 'gov3me']:
                party = winners[
                    winners['countryname'] == nations[nation]][govmem].tolist()
                if party:
                    party = party[0]
                    gov_parties.append(party)
            # Remove 'NA' from list of Gov Parties.
            gov_parties = [x for x in gov_parties if x != 'NA']
            # gov_parties is list of names of this years winners for this
            # country, convert to EB codes.
            gov_parties = get_eb_codes(gov_parties, nation)
            output[year][nation] = gov_parties

    # For France we want president rather than prime minister so we can't
    # use the DPI. Curated manually.
    # France coded as 1.
    # 1978 - 1980 REPUBLIC  : 11
    # 1980 - 1992 SOCIALIST : 200
    for i in range(1978, 1993):
        if (1978 <= i <= 1980):
            output[i][1] = [11]
        else:
            output[i][1] = [200]

    for key in sorted(output.keys()):
        print key, output[key]

    print 'Write File'
    outfile = 'lookups/Mannheim_gov_lookup.pickle'
    pickle.dump(output, open(outfile, 'wb'))


def get_eb_codes(gov_parties, nation):
    return_list = []
    for i in gov_parties:
        return_list = return_list + gov_lookup[nation][i]
    return return_list


if __name__ == '__main__':
    main()
