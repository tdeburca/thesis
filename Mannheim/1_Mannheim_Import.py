#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import pandas


def main():
    df = pandas.read_stata('ZA3521_v2-0-1.dta', convert_categoricals=False)

    gabel = ['age',
             'educ',
             'income',
             'nation1',
             'occup',
             'poldisc',
             'sex',
             'unifictn',
             'valpri1',
             'valpri2',
             'voteint',
             'year',
             'weuro',
             'membrshp']

    eb = [
        100,
        110,
        120,
        130,
        140,
        150,
        160,
        170,
        180,
        190,
        200,
        210,
        220,
        230,
        240,
        250,
        260,
        270,
        280,
        290,
        300,
        310,
        320,
        330,
        340,
        350,
        360,
        370]

    df = df[df['eb'].isin(eb)]
    # todos: L-RSELF-PLACEMENT:SCALE1-10

    # 1. FRANCE
    # 2. BELGIUM
    # 3. THE NETHERLANDS
    # 4. GERMANY WEST
    # 5. ITALY
    # 6. LUXEMBOURG
    # 7. DENMARK
    # 8. IRELAND
    # 9. GREAT BRITAIN
    # 10. NORTHERN IRELAND
    # 11. GREECE
    # 12. SPAIN
    # 13. PORTUGAL
    # 14. GERMANY EAST
    # 15. NORWAY
    founders = [1, 2, 3, 4, 5, 6]

    # We never care about:
    # 10. Northern Ireland
    # 14. Germany East
    # 15. Norway
    df = df[~df.nation1.isin([10, 14, 15])]

    # From Page 339, Note #8: I exclude surveys of nations that were not yet
    # members in the year surveyed.
    # Remove Greece before 1/1/1981.
    df = df.drop(df[(df.nation1 == 11) & (df.year < 1981)].index)
    # Remove Spain & Portugal before 1/1/1986.
    df = df.drop(df[(df.nation1.isin([12, 13])) & (df.year < 1986)].index)

    # Remove respondents younger than 18 as they cannot vote and so the
    # remaining questions don't make sense.
    df = df[df.age >= 18]

    # Only keep variables we need.
    df = df[gabel]

    # Add dummy variable fof nation1 (Added while we still have all the data for
    # consistency.)
    df = get_national_dummies(df)

    # Model 1: All Countries. '78 - 92
    pickle_survey(df, 'Model_1')
    # Model 2: Countries: 1,2,3,4,5,6 1978 <= 1986
    model2 = df[(df.year <= 1986) & (df.nation1.isin(founders))]
    pickle_survey(model2, 'Model_2')
    # Model 3: Countries: 1,2,3,4,5,6 1987 - '92
    model3 = df[(df.year > 1986) & (df.nation1.isin(founders))]
    pickle_survey(model3, 'Model_3')
    # Model 4: Countries: 7,8,9,11,12,13 1978 <= 1986
    model4 = df[(df.year <= 1986) & (~df.nation1.isin(founders))]
    pickle_survey(model4, 'Model_4')
    # Model 5: Countries: 7,8,9,11,12,13 1987 - '92
    model5 = df[(df.year > 1986) & (~df.nation1.isin(founders))]
    pickle_survey(model5, 'Model_5')


def get_national_dummies(df):
    nation_dummies = pandas.get_dummies(df.nation1)
    iso_countries = [
        'FR',
        'BE',
        'NL',
        'DE',
        'IT',
        'LU',
        'DK',
        'IE',
        'UK',
        'GR',
        'ES',
        'PT']
    nation_dummies.columns = iso_countries
    df = pandas.concat([df, nation_dummies], axis = 1)
    return df

def pickle_survey(data, filename):
        outfile='outfile/' + filename + '_import.pickle'
        data.to_pickle(outfile)
        print outfile

if __name__ == '__main__':
                                                                                                                                                                                                                                                                                                                                                                                                                        main()
