#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import pickle


def main():
    '''
    From the original paper:
    'The proletariat parties were
    (France) Communist Party, Socialist Party, and Lutte Ouvriere;
    (Belgium) Communist Party and Socialist Parties Walloon and Flemish;
    (The Netherlands) CPN and PVDA;
    (West Germany) Social Democratic Party;
    (Italy) PCI and PSI;
    (Luxembourg) KP and LSAP;
    (Denmark) DKP, SD, SPP, and Socialist Left;
    (Ireland) Labour Party and Workers' Party;
    (United Kingdom) Labour Party;
    (Greece) KKE, KKE international, and PA.SO.K;
    (Spain) PCI and PSOE;
    (Portugal) CDU and PSP.

    PCI not coded as PCI in EB Mannheim file,
    Unitad Communista and Partido Comunista seems correct.
    '''
    prol = {1: [100, 200, 101],
            2: [100, 200, 201],
            3: [100, 200],
            4: [200],
            5: [200, 200],
            6: [100, 200],
            7: [100, 200, 101, 201],
            8: [200, 201],
            9: [200],
            11: [101, 104, 200],
            12: [101, 102, 200],
            13: [100, 200]}

    '''
    The bourgeois parties were
    (France) RPR and UDF;
    (Belgium) PRL and Liberal Parties - Walloon and Flemish;
    (The Netherlands) CDA, and VVD;
    (West Germany) CDU/CSU and FDP;
    (Italy) CD and PLI;
    (Denmark) KF;
    (United Kingdom) Conservative Party;
    (Spain) CP;
    (Portugal) PDC and CDS.
    I was not able to identify a bourgeois party in Luxembourg or Ireland.
    --

    While the Author mentions being unable to find a Bourgeois party in LU and i
    IE he is also missing a party for GR with no mention in the text.
    '''
    bour = {1: [402, 403, 405, 500],
            2: [300, 301, 305, 307],
            3: [400, 300],
            4: [400, 300],
            5: [400, 301],
            7: [500],
            9: [500],
            12: [101, 102, 100],
            13: [400, 201]}

    cp = {'prol': prol, 'bour': bour}

    print 'Write File'
    outfile = 'lookups/Mannheim_prol_bour.pickle'
    pickle.dump(cp, open(outfile, 'wb'))


if __name__ == '__main__':
    main()
