Requirements:
This code was run and tested with the following library versions, it is likely to work with newer versions, but tested versions are shown here. 

* pandas==0.20.1
* statsmodels==0.8.0
* scikit-learn==0.18.1

Creation of lookup tables:
* Mannheim_gov_lookup.py
* Mannheim_prol_bour_lookup.py

These files created lookup tables, stored in 'lookups' which allow the mapping between Voter Intention and Support for Government, and Voter Intention and Class Partisanship.

To reproduce the Mannheim model run the files in numerical order:
* 1_Mannheim_Import.py - Extracts all needed data from Mannheim Trend File.
* 2_Mannheim_Vars.py - Convers variables into desired format. (Dummys etc.)
* 3_Mannheim_regressions.py - Runs Regression, outputs coefficients. 

Datafiles:
* ZA3521_cod_v2-0-1.pdf - Mannheim Codebook
* ZA3521_cod_appendix_v2-0-1.pdf - Codebook Appendix
* ZA3521_v2-0-1.dta - Mannheim Data File, Stata format.

