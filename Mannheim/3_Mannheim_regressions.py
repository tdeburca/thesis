#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tdeburca@gmail.com>
# Be a terrible human being and disable warnings.
import warnings
warnings.filterwarnings('ignore')

# Necessary imports
import pandas
import pickle
import statsmodels.api as sm
from sklearn.preprocessing import MinMaxScaler

basefile = 'outfile/Model_{}_vars.pickle'


def main():
    models = {
        1: ['UK', 1989],
        2: ['IT', 1986],
        3: ['IT', 1989],
        4: ['UK', 1986, 'ES', 'PT'],
        5: ['UK', 1989]}

    for model in models:
        # "The baseline respondent in each model has the following
        # characteristics:  "occasionally" discusses political matters with
        # friends; has "mixed" political values works in a white-collar
        # profession; finished school between age 14 and
        # 20 falls in the second-to-lowest national income quartile, and reside
        # in a non-border region. The baseline year and nation differ across
        # models so that the largest national and annual sample serves as
        # the omitted categories. "
        # These are consisent across all baselines. Rest added below.
        baseline = [
            'white_collar',
            'educ_low_mid',
            'inc_low_mid',
            'cm_occassionally']
        baseline.extend(models[model])
        print baseline
        res = regression_from_pickle(model, baseline)
        print res.summary()
        print 'p-values greater than .05. Result suspicious.'
        print res.pvalues[res.pvalues > 0.05]
        easy_paste(res, baseline)
        print 'Write File'
        outfile = 'outfile/Model_{}_result.pickle'.format(model)
        pickle.dump(res, open(outfile, 'wb'))


def easy_paste(res, baseline):
    exp_vars = ['cm_never',
                'cm_frequently',
                'materialist',
                'postmaterialist',
                'professional',
                'executive',
                'manual_worker',
                'unemployed',
                'educ_low',
                'educ_high_mid',
                'educ_high',
                'inc_low',
                'inc_high_mid',
                'inc_high',
                'prol',
                'bour',
                'support_gov']
    for i in exp_vars:
        print i, res.params[i]
    control_vars = res.params.keys()
    control_vars = set(control_vars) - set(exp_vars)
    control_vars = set(control_vars) - set(baseline)
    print '========'
    print '========'
    for i in control_vars:
        print i, res.params[i]
    print '========'
    print '========'
    raw_input('Press <ENTER> to continue')


def regression_from_pickle(modelnumber, baseline):
    # Load Data.
    df = pandas.read_pickle(open(basefile.format(modelnumber)))

    # Get and Scale Dependent Var. (Support)
    y = pandas.DataFrame.pop(df, 'support')
    scaler = MinMaxScaler(feature_range=(0, 100))
    y = scaler.fit_transform(y)

    # Get Weights
    weights = pandas.DataFrame.pop(df, 'weuro')
    # To match terminology, Explanatory/independent/experimental/predictor
    # called X.
    X = df
    # Adds var 'const' as it's required for the regression.
    X = sm.add_constant(X)
    # To remove some multicolinearity from the model we remove a baseline.
    X = X.drop(baseline, axis=1)

    wls_model = sm.WLS(y, X, weights=weights)
    res = wls_model.fit(cov_type='HC0')
    return res

if __name__ == '__main__':
    main()
