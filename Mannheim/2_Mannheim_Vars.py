#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Tiarnan de Burca <tiarnan.deburca@heanet.ie>

import pandas
import pickle
# Var created by Mannheim_gov_lookup.py
# Format gov_lookup[year][nation] : Code for ruling party.
gov_lookup = pickle.load(open('lookups/Mannheim_gov_lookup.pickle'))
# Var created by Mannheim_prol_bour.py
# cp[prol][nation] : list of prol parties, eb coded.
# cp[bour][nation] : list of bour parties, eb coded.
cp = pickle.load(open('lookups/Mannheim_prol_bour.pickle'))

'''
This disables this warning:
prep_explanatory_vars.py:58: SettingWithCopyWarning:
A value is trying to be set on a copy of a slice from a DataFrame.
Try using .loc[row_indexer,col_indexer] = value instead
'''
pandas.set_option('chained_assignment', None)


def main():
    for model in range(1, 6):
        print 'Model', model
        df = pandas.read_pickle('outfile/Model_{}_import.pickle'.format(model))
        df = prep_regression(df)
        print "Writing Model Vars", model
        df.to_pickle('outfile/Model_{}_vars.pickle'.format(model))
        print df.shape
        print df.columns


def prep_regression(df):
    # Adds Support Variable.
    df = get_support(df)
    # Adds Cognitive Mobilization variables.
    df = get_cm(df)
    # Adds Political Values
    df = get_pv(df)
    # Utilitarian
    # Add Education Quartile.
    df = get_educ(df)
    # Add Income Quartile
    df = get_income(df)
    # Add Dummys for Occupation.
    df = get_occupation(df)
    # VoteInt used twice so cleaned in its own function to avoid confusion.
    df = clean_voteint(df)
    df = get_support_gov(df)
    df = get_bour_prol(df)
    # Drop Voteint as we're finished with it.
    df.drop('voteint', axis=1, inplace=True)
    # Add Female Dummy Variable.
    df = get_sex(df)
    # Add dummy var per year.
    df = get_year(df)
    # Drop Nation1 as we're finished with it. (Dummies added for regression
    # earlier.)
    df.drop('nation1', axis=1, inplace=True)
    # Drop Empty Columns. (In effect drop the dummy columns for countries not in
    # this model.)
    df = df.loc[:, (df != 0).any(axis=0)]
    # Normalize age.
    df = normalize_age(df)
    return df


def normalize_age(df):
    age = df['age']
    age_norm = (age - age.mean()) / (age.max() - age.min())
    df['age'] = age_norm
    return df


def get_bour_prol(df):
    df['prol'] = df.apply(prol_map, axis=1)
    df['bour'] = df.apply(bour_map, axis=1)
    return df


def prol_map(x):
    if x.voteint in cp['prol'][x.nation1]:
        return 1
    else:
        return 0


def bour_map(x):
    # Author mentions being unable to find a Bourgeois party in LU(6) and IE(8)
    # he is also missing a party for GR(11) with no mention in the text.
    if x.nation1 in [6, 8, 11]:
        return 0
    if x.voteint in cp['bour'][x.nation1]:
        return 1
    else:
        return 0


def get_support_gov(df):
    # return 1 in support for gov if respondent would vote for gov party.
    df['support_gov'] = df.apply(support_gov_map, axis=1)
    return df


def support_gov_map(x):
    if x.voteint in gov_lookup[x.year][x.nation1]:
        return 1
    else:
        return 0
    return x.year


def clean_voteint(df):
    # Correct handling of this variable is non-obvious. Should non voters count.
    # Might be non-citizens?
    # ZEUS Code 0 = NA
    df = df[df.voteint != 0]
    # 990 >= DK/NA
    df = df[df.voteint <= 989]
    return df


def get_year(df):
    df = pandas.concat([df, pandas.get_dummies(df.year)], axis=1)
    df.drop('year', axis=1, inplace=True)
    return df


def get_sex(df):
    # DK/NA recorded as 8, dropping.
    df = df[df.sex != 8]
    df['female'] = pandas.get_dummies(df.sex)[2]
    # Drop sex as no longer needed.
    df.drop('sex', axis=1, inplace=True)
    return df


def get_occupation(df):
    # Cleanup. Drop DK/NA >998.0
    df = df[df.occup < 998]
    df.occup = df.occup.apply(occup_map)
    occup = pandas.get_dummies(df.occup)
    occup.columns = [
        'farmer',
        'professional',
        'owner',
        'manual_worker',
        'white_collar',
        'executive',
        'retired',
        'housewife',
        'student_mil',
        'unemployed']
    df = pandas.concat([df, occup], axis=1)
    df.drop('occup', axis=1, inplace=True)
    return df


def occup_map(x):
    if x in [110, 111, 112]:
        return 1
    if x in [120, 210]:
        return 2
    if x in [130, 131, 132]:
        return 3
    if x in [410, 411, 412, 413]:
        return 4
    if x in [230, 310, 311, 312, 320, 321, 322]:
        return 5
    if x in [220]:
        return 6
    if x in [530]:
        return 7
    if x in [510]:
        return 8
    if x in [500, 520, 521, 522]:
        return 9
    if x in [540]:
        return 10


def get_income(df):
    df = get_income_lvls(df)
    # Dummy Vars for valid answers.
    inc = pandas.get_dummies(df.income)
    inc.columns = ['inc_low', 'inc_low_mid', 'inc_high_mid', 'inc_high']
    df = pandas.concat([df, inc], axis=1)
    df.drop('income', axis=1, inplace=True)
    return df


def get_income_lvls(df):
    # Cleanup. Drop NA. Drop DK/NA (96+)
    df = df.dropna(subset=['income'], how='any')
    # 95 and greater means DK/NA drop
    df = df[df.income < 95]
    df['income'] = df.groupby('nation1').income.transform(
        lambda x: pandas.qcut(x, 4, labels=False, duplicates='drop'))
    return df


def get_educ(df):
    # Re-Rank Education into 4 levels from Paper.
    df = get_educ_lvls(df)
    # Dummy Vars for valid answers.
    educ = pandas.get_dummies(df.educ)
    educ.columns = ['educ_low', 'educ_low_mid', 'educ_high_mid', 'educ_high']
    df = pandas.concat([df, educ], axis=1)
    df.drop('educ', axis=1, inplace=True)
    return df


def get_educ_lvls(df):
    # Greater than 90 is DK/NA.
    df = df[df.educ < 90]
    df['educ'] = df.apply(educ_map, axis=1)
    return df


def educ_map(x):
    if x.educ == 1:
        return 1
    if 2 <= x.educ <= 6:
        return 2
    if 7 <= x.educ <= 8:
        return 3
    if x.educ == 9:
        return 4
    if x.educ == 10:
        if x.age in [18, 19]:
            return 2
        if x.age in [20, 21]:
            return 3
        if x.age >= 22:
            return 4
    print 'AgeError:', x
    quit()


def get_cm(df):
    # Drop people who didn't answer the question.
    df = df[df.poldisc != 8]
    # Dummy Vars for valid answers.
    cm = pandas.get_dummies(df.poldisc)
    cm.columns = ['cm_frequently', 'cm_occassionally', 'cm_never']
    df = pandas.concat([df, cm], axis=1)
    df.drop('poldisc', axis=1, inplace=True)
    return df


def get_pv(df):
    # Drop people who didn't answer the question.
    df = df.dropna(subset=['valpri1', 'valpri2'], how='any')
    # 8 means DK/NA drop
    df = df[df.valpri1 != 8]
    df = df[df.valpri2 != 8]

    '''
    Values and their encoding.
    1. MAINTENANCE OF LAW AND ORDER
    2. GIVING THE PEOPLE MORE SAY IN GOVERNMENT DECISIONS
    3. FIGHTING RISING PRICES
    4. PROTECTING FREEDOM OF EXPRESSION
    '''
    materailist = [1, 3]
    postmaterialist = [2, 4]
    # there are cases where the respondant answers the same for both.
    # these are scored as two votes for the same value.
    # i.e. 'Freedom' twice counts as 'Post Materialist'
    df['materialist'] = (
        df['valpri1'].isin(materailist)) & (
        df['valpri2'].isin(materailist))
    df['postmaterialist'] = (
        df['valpri1'].isin(postmaterialist)) & (
        df['valpri2'].isin(postmaterialist))

    # Switch from boolean to numeric so we can perform regression.
    df.materialist = df.materialist.astype(int)
    df.postmaterialist = df.postmaterialist.astype(int)
    # valpri vars no longer needed.
    df.drop(['valpri1', 'valpri2'], axis=1, inplace=True)
    return df


def get_support(df):
    # membrshp
    # Responses of "don't know" were excluded from the analysis since
    # respondents could express indifference through the intermediate category
    # Mannheim encodes "don't know" as '8'.
    df = df.drop(df[df.membrshp == 8].index)
    # This is ugly, look for a better method
    # We need to recode 3 as 1 and 1 as 3.
    df.membrshp.replace(to_replace=1, value='three', inplace=True)
    df.membrshp.replace(to_replace=3, value='one', inplace=True)
    df.membrshp.replace(to_replace='three', value=3, inplace=True)
    df.membrshp.replace(to_replace='one', value=1, inplace=True)

    # unifictn
    # Don't know to be recoded from 8 to 2.5.
    df.unifictn.replace(to_replace=8, value=2.5, inplace=True)
    # We need to recode 1 as 4, 2 as 3, 3 as 2, 4 as 1.
    df.unifictn.replace(to_replace=1, value='four', inplace=True)
    df.unifictn.replace(to_replace=2, value='three', inplace=True)
    df.unifictn.replace(to_replace=3, value='two', inplace=True)
    df.unifictn.replace(to_replace=4, value='one', inplace=True)
    df.unifictn.replace(to_replace='one', value=1, inplace=True)
    df.unifictn.replace(to_replace='two', value=2, inplace=True)
    df.unifictn.replace(to_replace='three', value=3, inplace=True)
    df.unifictn.replace(to_replace='four', value=4, inplace=True)

    # support = membrshp + unifictn
    df['support'] = df['membrshp'] + df['unifictn']
    df.drop(['membrshp', 'unifictn'], axis=1, inplace=True)
    return df


if __name__ == '__main__':
    main()
